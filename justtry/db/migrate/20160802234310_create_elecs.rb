class CreateElecs < ActiveRecord::Migration
  def change
    create_table :elecs do |t|
      t.string :class
      t.string :brand
      t.references :people, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
