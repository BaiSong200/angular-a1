class LendersController < ApplicationController
  before_action :find_lender, only: [:edit,:show,:update,:destroy]
  def index
  end

  def show
  end

  def new
    @lender = Lender.new
  end

  def create
  end

  def edit
  end

  def update
  end

  def delete
  end

  private
  def lender_params
    params.require(:lender).permit(:username, :note)
  end

  def find_lender
    @lender = Lender.find(params[:id])
  end
end
