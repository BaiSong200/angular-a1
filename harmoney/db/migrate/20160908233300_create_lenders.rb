class CreateLenders < ActiveRecord::Migration
  def change
    create_table :lenders do |t|
      t.string :username
      t.decimal :deposit
      t.integer :note

      t.timestamps null: false
    end
  end
end
